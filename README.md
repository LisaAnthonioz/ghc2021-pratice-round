# GOOGLE HASH CODE 2021 - PRACTICE ROUND

## V1

| **700839359** | **ALL**                    |
| ------------- | -------------------------- |
| 65            | a_example                  |
| 5612          | b_little_bit_of_everything |
| 686663538     | c_many_ingredients         |
| 5852236       | d_many_pizzas              |
| 8317908       | e_many_teams               |

It tooks 960 ms in java to generate the output files.
It tooks 227624 ms (4min) in java to calculate the scores.

It tooks 8711 ms in python to generate the output files.
It tooks 7536424 ms (2h) in python to calculate the scores.

It tooks 5580 ms in typescript to generate the output files.
It tooks 100285 ms (1,5min) in typescript to calculate the scores.
