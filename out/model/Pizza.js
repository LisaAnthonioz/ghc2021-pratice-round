"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pizza = void 0;
var Pizza = /** @class */ (function () {
    function Pizza(id, numberOfIngredients, ingredients) {
        this.id = id;
        this.numberOfIngredients = numberOfIngredients;
        this.ingredients = ingredients;
    }
    Pizza.prototype.getId = function () {
        return this.id;
    };
    Pizza.prototype.getNumberOfIngredients = function () {
        return this.numberOfIngredients;
    };
    Pizza.prototype.getIngredients = function () {
        return this.ingredients;
    };
    Pizza.prototype.setId = function (id) {
        this.id = id;
    };
    Pizza.prototype.setNumberOfIngredients = function (nb) {
        this.numberOfIngredients = nb;
    };
    Pizza.prototype.setIngredients = function (ingredients) {
        this.ingredients = ingredients;
    };
    Pizza.prototype.addIngredientsDiff = function (p) {
        var somme = this.numberOfIngredients;
        if (this.numberOfIngredients >= p.getNumberOfIngredients()) {
            for (var _i = 0, _a = p.getIngredients(); _i < _a.length; _i++) {
                var ingredient = _a[_i];
                if (!this.ingredients.includes(ingredient)) {
                    somme += 1;
                }
            }
        }
        else {
            for (var _b = 0, _c = this.ingredients; _b < _c.length; _b++) {
                var ingredient = _c[_b];
                if (!p.getIngredients().includes(ingredient)) {
                    somme += 1;
                }
            }
        }
        return somme;
    };
    return Pizza;
}());
exports.Pizza = Pizza;
//# sourceMappingURL=Pizza.js.map