package model;

import java.util.ArrayList;
import java.util.List;

public class Pizza implements Comparable<Pizza> {
    private int id;
    private int numberOfIngredients;
    private List<String> ingredients;

    public Pizza(int numberOfIngredients, List<String> ingredients) {
        this.numberOfIngredients = numberOfIngredients;
        this.ingredients = ingredients;
    }

    public Pizza(int id, int numberOfIngredients) {
        this.id = id;
        this.numberOfIngredients = numberOfIngredients;
        this.ingredients = new ArrayList<String>();
    }

    public int getId() {
        return this.id;
    }

    public int getNumberOfIngredients() {
        return this.numberOfIngredients;
    }

    public void setNumberOfIngredients(int numberOfIngredients) {
        this.numberOfIngredients = numberOfIngredients;
    }

    public List<String> getIngredients() {
        return this.ingredients;
    }

    @Override
    public int compareTo(Pizza p) {
        return p.getNumberOfIngredients() - this.numberOfIngredients;
    }

    public int addIngredientsDiff(Pizza p) {
        int somme = this.numberOfIngredients;

        if (this.numberOfIngredients >= p.getNumberOfIngredients()) {
            for (String ingredient : p.getIngredients()) {
                if (!this.ingredients.contains(ingredient)) {
                    somme += 1;
                }
            }
        } else {
            for (String ingredient : this.ingredients) {
                if (!p.getIngredients().contains(ingredient)) {
                    somme += 1;
                }
            }
        }

        return somme;
    }
}
