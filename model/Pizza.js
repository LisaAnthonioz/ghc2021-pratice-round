"use strict";
exports.__esModule = true;
exports.Pizza = void 0;
var Pizza = /** @class */ (function () {
    function Pizza(id, numberOfIngredients, ingredients) {
        this.id = id;
        this.numberOfIngredients = numberOfIngredients;
        this.ingredients = ingredients;
    }
    Pizza.prototype.getId = function () {
        return this.id;
    };
    Pizza.prototype.getNumberOfIngredients = function () {
        return this.numberOfIngredients;
    };
    Pizza.prototype.getIngredients = function () {
        return this.ingredients;
    };
    return Pizza;
}());
exports.Pizza = Pizza;
