export class Pizza {
    id: number;
    numberOfIngredients: number;
    ingredients: string[];

    constructor(id: number, numberOfIngredients: number, ingredients: string[]) {
        this.id = id;
        this.numberOfIngredients = numberOfIngredients;
        this.ingredients = ingredients;
    }


    getId() {
        return this.id;
    }

    getNumberOfIngredients() {
        return this.numberOfIngredients;
    }

    getIngredients(): string[] {
        return this.ingredients;
    }

    setId(id: number) {
        this.id = id;
    }

    setNumberOfIngredients(nb: number) {
        this.numberOfIngredients = nb;
    }

    setIngredients(ingredients: string[]) {
        this.ingredients = ingredients;
    }

    addIngredientsDiff(p: Pizza): any {
        let somme = this.numberOfIngredients;

        if (this.numberOfIngredients >= p.getNumberOfIngredients()) {
            for (let ingredient of p.getIngredients()) {
                if (!this.ingredients.includes(ingredient)) {
                    somme += 1;
                }
            }
        } else {
            for (let ingredient of this.ingredients) {
                if (!p.getIngredients().includes(ingredient)) {
                    somme += 1;
                }
            }
        }

        return { nb: somme, id: p.id };
    }
}
