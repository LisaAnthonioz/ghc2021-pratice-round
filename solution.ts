import { Pizza } from "./model/Pizza";
import { readFileSync, writeFileSync } from 'fs';

class Main {

    inputFilesDirectory = "input/"
    outputFilesDirectory = "output/"

    fileNames = ["a_example", "b_little_bit_of_everything", "c_many_ingredients", "d_many_pizzas", "e_many_teams"];
    // fileNames = ["c_many_ingredients"];

    NUMBER_OF_PIZZAS = 0;
    NUMBER_OF_2_PERSON_TEAMS = 0;
    NUMBER_OF_3_PERSON_TEAMS = 0;
    NUMBER_OF_4_PERSON_TEAMS = 0;

    read(fileName): Pizza[] {
        // Read the file
        let data = readFileSync(this.inputFilesDirectory + fileName + ".in").toString('utf-8');
        let lines = data.split('\n').slice(0, -1);
        let firstLine = lines[0].trim();
        let pizzas = [];
        if (firstLine) {
            //  Set the constants
            let consts = firstLine.split(' ');
            this.NUMBER_OF_PIZZAS = +consts[0];
            this.NUMBER_OF_2_PERSON_TEAMS = +consts[1];
            this.NUMBER_OF_3_PERSON_TEAMS = +consts[2];
            this.NUMBER_OF_4_PERSON_TEAMS = +consts[3];
        }
        let idPizza = 0
        for (let line of lines.slice(1)) {
            //   Set the pizzas
            let values = line.split(" ");
            let pizza = new Pizza(idPizza, +(values[0]), [])
            for (let i of values.slice(1)) {
                pizza.ingredients.push(i);
            }
            pizzas.push(pizza)
            idPizza += 1
        }
        return pizzas;
    }

    process(pizzas: Pizza[], fileName: string) {

        let nbPizzasRestantes = this.NUMBER_OF_PIZZAS
        let nbOf4PersonTeamServed = 0
        let nbOf3PersonTeamServed = 0
        let nbOf2PersonTeamServed = 0

        while (nbPizzasRestantes > 0 && (nbOf3PersonTeamServed < this.NUMBER_OF_3_PERSON_TEAMS || nbOf4PersonTeamServed < this.NUMBER_OF_4_PERSON_TEAMS)) {

            if (nbPizzasRestantes > 1) {

                if (nbPizzasRestantes > this.NUMBER_OF_4_PERSON_TEAMS * 4) {
                    nbOf4PersonTeamServed = this.NUMBER_OF_4_PERSON_TEAMS
                    nbPizzasRestantes = nbPizzasRestantes - this.NUMBER_OF_4_PERSON_TEAMS * 4

                } else if (nbPizzasRestantes > 0 && nbPizzasRestantes % 4 == 0) {

                    nbOf4PersonTeamServed = +(nbPizzasRestantes / 4)
                    nbPizzasRestantes = 0
                }

                if (nbPizzasRestantes > this.NUMBER_OF_3_PERSON_TEAMS * 3) {
                    nbOf3PersonTeamServed = this.NUMBER_OF_3_PERSON_TEAMS
                    nbPizzasRestantes = nbPizzasRestantes - this.NUMBER_OF_3_PERSON_TEAMS * 3

                } else if (nbPizzasRestantes > 0 && nbPizzasRestantes % 3 == 0) {
                    nbOf3PersonTeamServed = +(nbPizzasRestantes / 3)
                    nbPizzasRestantes = 0;
                }

                if (nbPizzasRestantes > this.NUMBER_OF_2_PERSON_TEAMS * 2) {
                    nbOf2PersonTeamServed = this.NUMBER_OF_2_PERSON_TEAMS
                    nbPizzasRestantes = nbPizzasRestantes - this.NUMBER_OF_2_PERSON_TEAMS * 2

                } else if (nbPizzasRestantes > 0 && nbPizzasRestantes % 2 == 0) {
                    nbOf2PersonTeamServed = +(nbPizzasRestantes / 2)
                    nbPizzasRestantes = 0;
                }

            }
            else {
                nbOf4PersonTeamServed -= 1

                nbPizzasRestantes += 4
                if (nbPizzasRestantes > this.NUMBER_OF_3_PERSON_TEAMS * 3) {
                    nbOf3PersonTeamServed = this.NUMBER_OF_3_PERSON_TEAMS
                    nbPizzasRestantes = nbPizzasRestantes - this.NUMBER_OF_3_PERSON_TEAMS * 3
                } else if (nbPizzasRestantes > 0 && nbPizzasRestantes % 3 == 0) {
                    nbOf3PersonTeamServed = +(nbPizzasRestantes / 3)
                }

                if (nbPizzasRestantes > this.NUMBER_OF_2_PERSON_TEAMS * 2) {
                    nbOf2PersonTeamServed = this.NUMBER_OF_2_PERSON_TEAMS
                    nbPizzasRestantes = nbPizzasRestantes - this.NUMBER_OF_2_PERSON_TEAMS * 2
                } else if (nbPizzasRestantes > 0 && nbPizzasRestantes % 2 == 0) {
                    nbOf2PersonTeamServed = +(nbPizzasRestantes / 2)
                    nbPizzasRestantes = nbPizzasRestantes - this.NUMBER_OF_2_PERSON_TEAMS * 2

                }
            }
        }
        // #  SERVE THE PIZZAS
        let nbTeamsDelivered = nbOf4PersonTeamServed + nbOf3PersonTeamServed + nbOf2PersonTeamServed;
        let outputFileName = this.outputFilesDirectory + fileName + "-ts.out";
        let data = nbTeamsDelivered.toString() + '\n';
        for (let i = 0; i < nbOf4PersonTeamServed; i++) {
            data += "4 ";
            let ids = this.getBestPizza(pizzas, 4);
            data += ids[0] + " " + ids[1] + " " + ids[2] + " " + ids[3] + "\n";
        }
        for (let i = 0; i < nbOf3PersonTeamServed; i++) {
            data += "3 ";
            let ids = this.getBestPizza(pizzas, 3);
            data += ids[0] + " " + ids[1] + " " + ids[2] + "\n";
        }
        for (let i = 0; i < nbOf2PersonTeamServed; i++) {
            data += "2 ";
            let ids = this.getBestPizza(pizzas, 2);

            data += ids[0] + " " + ids[1] + "\n";
        }
        writeFileSync(outputFileName, data);

    }

    getBestPizza(pizzas: Pizza[], nb: number): number[] {
        let pizza0 = pizzas[0];
        let ids = [pizzas[0].getId()];
        pizzas.splice(0, 1)
        for (let i = 1; i < nb; i++) {
            let max = [];
            pizzas.forEach(p => {
                max.push(pizza0.addIngredientsDiff(p));
            });

            pizza0 = pizzas.find(p => p.id == max.reduce(function (prev, current) {
                return (prev.nb > current.nb) ? prev : current
            }).id);

            pizzas.splice(pizzas.findIndex(p => p.id == pizza0.id), 1);
            ids.push(pizza0.getId());
        }
        return ids;
    }

    displayScore(fileNames: string[]) {
        let scoreTotal = 0
        for (let fileName of fileNames) {

            let pizzas = this.read(fileName)
            let score = 0

            // Read the file
            let data = readFileSync(this.outputFilesDirectory + fileName + "-ts.out").toString('utf-8');
            let lines = data.split("\n").slice(0, -1);
            for (let line of lines.slice(1)) {
                let ingredientsByTeam = []
                let values = line.split(" ")
                for (let val of values.slice(1)) {
                    for (let pizza of pizzas) {
                        if (pizza.id == +val) {
                            ingredientsByTeam = ingredientsByTeam.concat(pizza.ingredients.filter(f => !ingredientsByTeam.includes(f)));
                        }
                    }
                }
                score += ingredientsByTeam.length * ingredientsByTeam.length
            }
            scoreTotal += score

            console.log("SCORE FOR " + fileName + ": " + score)
        }
        console.log("SCORE TOTAL : " + scoreTotal)


    }
    main() {
        let start_time = new Date().getTime();
        console.log("Start : ", new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
        for (let fileName of this.fileNames) {
            let pizzas = this.read(fileName)
            pizzas.sort((a, b) => b.getNumberOfIngredients() - a.getNumberOfIngredients());
            this.process(pizzas, fileName)
            console.log("Time process " + fileName + ": ", new Date().getTime() - start_time, "ms, soit " +
                (new Date().getTime() - start_time) / 60000 + " min");
        }
        console.log("Time before score calculation: ", new Date().getTime() - start_time, "ms, soit " +
            (new Date().getTime() - start_time) / 60000 + " min");
        this.displayScore(this.fileNames);
        console.log("Time after score calculation: ", new Date().getTime() - start_time, "ms, soit " +
            (new Date().getTime() - start_time) / 60000 + " min");

        console.log("End : ", new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds());
    }


}

let main = new Main();
main.main();