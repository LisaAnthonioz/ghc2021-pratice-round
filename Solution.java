import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import model.Pizza;

class Solution {

    static int NUMBER_OF_PIZZAS;
    static int NUMBER_OF_2_PERSON_TEAMS;
    static int NUMBER_OF_3_PERSON_TEAMS;
    static int NUMBER_OF_4_PERSON_TEAMS;
    static String inputFilesDirectory = "./input/";
    static String outputFilesDirectory = "./output/";

    public static void main(String[] args) throws IOException {

        String[] fileNames = { "a_example", "b_little_bit_of_everything", "c_many_ingredients", "d_many_pizzas",
                "e_many_teams" };

        long start_time = System.currentTimeMillis();
        System.out.println("Start : " + (System.currentTimeMillis() - start_time) + " ms, soit "
                + (System.currentTimeMillis() - start_time) * 0.003 + " min");

        for (String fileName : fileNames) {
            ArrayList<Pizza> pizzas = read(fileName);
            // displayInputData(pizzas, fileName);
            Collections.sort(pizzas);
            process(pizzas, fileName);
            System.out.println("Time process " + fileName + ": " + (System.currentTimeMillis() - start_time)
                    + " ms, soit " + (System.currentTimeMillis() - start_time) * 0.003 + " min");
        }

        System.out.println("Time before score calculation: " + (System.currentTimeMillis() - start_time) + " ms, soit "
                + (System.currentTimeMillis() - start_time) * 0.003 + " min");
        displayScore(fileNames);
        System.out.println("Time after score calculation: " + (System.currentTimeMillis() - start_time) + " ms, soit "
                + (System.currentTimeMillis() - start_time) * 0.003 + " min");
    }

    private static ArrayList<Pizza> read(String fileName) throws IOException {
        ArrayList<Pizza> pizzas = new ArrayList<Pizza>();
        // Read the file
        BufferedReader reader = new BufferedReader(new FileReader(inputFilesDirectory + fileName + ".in"));

        // Set the constants
        String firstLine = reader.readLine();
        String[] vars = firstLine.split(" ");

        NUMBER_OF_PIZZAS = Integer.valueOf(vars[0]);
        NUMBER_OF_2_PERSON_TEAMS = Integer.valueOf(vars[1]);
        NUMBER_OF_3_PERSON_TEAMS = Integer.valueOf(vars[2]);
        NUMBER_OF_4_PERSON_TEAMS = Integer.valueOf(vars[3]);

        // Set the pizzas
        String line;
        Integer idPizza = 0;
        while ((line = reader.readLine()) != null) {
            String values[] = line.split(" ");
            Pizza pizza = new Pizza(idPizza, Integer.valueOf(values[0]));
            for (int i = 1; i < values.length; i++) {
                pizza.getIngredients().add(values[i]);
            }
            pizzas.add(pizza);
            idPizza++;
        }

        // Close the reader
        reader.close();

        return pizzas;
    }

    private static void displayInputData(List<Pizza> pizzas, String fileName) {
        // Display input data
        System.out.println("---------------------------------------------------------------");
        System.out.println(fileName);
        System.out.println("---------------------------------------------------------------");
        System.out.println(NUMBER_OF_PIZZAS + " pizzas, " + NUMBER_OF_2_PERSON_TEAMS + " team(s) of two, "
                + NUMBER_OF_3_PERSON_TEAMS + " team(s) of three, " + NUMBER_OF_4_PERSON_TEAMS + " team(s) of four");

        if (NUMBER_OF_PIZZAS < 10) {
            System.out.println("---------------------------------------------------------------");
            for (int i = 0; i < NUMBER_OF_PIZZAS; i++) {
                System.out.print("Pizza " + i + " has " + pizzas.get(i).getNumberOfIngredients() + " ingredients : ");
                for (int j = 0; j < pizzas.get(i).getNumberOfIngredients(); j++) {
                    System.out.print(pizzas.get(i).getIngredients().get(j) + ", ");
                }
                System.out.println("");
            }
        }
        System.out.println("---------------------------------------------------------------");
    }

    private static void process(List<Pizza> pizzas, String fileName) throws IOException {
        int nbPizzasRestantes = NUMBER_OF_PIZZAS;
        int nbOf4PersonTeamServed = 0;
        int nbOf3PersonTeamServed = 0;
        int nbOf2PersonTeamServed = 0;

        while (nbPizzasRestantes > 0 && (nbOf3PersonTeamServed < NUMBER_OF_3_PERSON_TEAMS
                || nbOf4PersonTeamServed < NUMBER_OF_4_PERSON_TEAMS)) {

            if (nbPizzasRestantes > 1) {
                if (nbPizzasRestantes > NUMBER_OF_4_PERSON_TEAMS * 4) {
                    nbOf4PersonTeamServed = NUMBER_OF_4_PERSON_TEAMS;
                    nbPizzasRestantes -= NUMBER_OF_4_PERSON_TEAMS * 4;
                } else if (nbPizzasRestantes > 0 && nbPizzasRestantes % 4 == 0) {
                    nbOf4PersonTeamServed = nbPizzasRestantes / 4;
                    nbPizzasRestantes = 0;
                }

                if (nbPizzasRestantes > NUMBER_OF_3_PERSON_TEAMS * 3) {
                    nbOf3PersonTeamServed = NUMBER_OF_3_PERSON_TEAMS;
                    nbPizzasRestantes -= NUMBER_OF_3_PERSON_TEAMS * 3;
                } else if (nbPizzasRestantes > 0 && nbPizzasRestantes % 3 == 0) {
                    nbOf3PersonTeamServed = nbPizzasRestantes / 3;
                    nbPizzasRestantes = 0;
                }

                if (nbPizzasRestantes > NUMBER_OF_2_PERSON_TEAMS * 2) {
                    nbOf2PersonTeamServed = NUMBER_OF_2_PERSON_TEAMS;
                    nbPizzasRestantes -= NUMBER_OF_2_PERSON_TEAMS * 2;
                } else if (nbPizzasRestantes > 0 && nbPizzasRestantes % 2 == 0) {
                    nbOf2PersonTeamServed = nbPizzasRestantes / 2;
                    nbPizzasRestantes = 0;
                }
            } else {
                nbOf4PersonTeamServed -= 1;
                nbPizzasRestantes += 4;

                if (nbPizzasRestantes > NUMBER_OF_3_PERSON_TEAMS * 3) {
                    nbOf3PersonTeamServed = NUMBER_OF_3_PERSON_TEAMS;
                    nbPizzasRestantes -= NUMBER_OF_3_PERSON_TEAMS * 3;
                } else if (nbPizzasRestantes > 0 && nbPizzasRestantes % 3 == 0) {
                    nbOf3PersonTeamServed = nbPizzasRestantes / 3;
                    nbPizzasRestantes = 0;
                }

                if (nbPizzasRestantes > NUMBER_OF_2_PERSON_TEAMS * 2) {
                    nbOf2PersonTeamServed = NUMBER_OF_2_PERSON_TEAMS;
                    nbPizzasRestantes -= NUMBER_OF_2_PERSON_TEAMS * 2;
                } else if (nbPizzasRestantes > 0 && nbPizzasRestantes % 2 == 0) {
                    nbOf2PersonTeamServed = nbPizzasRestantes / 2;
                    nbPizzasRestantes = 0;
                }
            }
        }

        // SERVE THE PIZZAS
        int nbTeamsDelivered = nbOf4PersonTeamServed + nbOf3PersonTeamServed + nbOf2PersonTeamServed;

        FileWriter writer = new FileWriter(outputFilesDirectory + fileName + ".out");
        writer.write(String.valueOf(nbTeamsDelivered) + "\n");

        for (int i = 0; i < nbOf4PersonTeamServed; i++) {
            Pizza pizzaTeam = new Pizza(pizzas.get(0).getNumberOfIngredients(), pizzas.get(0).getIngredients());
            writer.write("4 " + pizzas.get(0).getId() + " ");
            pizzas.remove(0);

            int idPizza1 = getBestPizza(pizzas, pizzaTeam);
            int idPizza2 = getBestPizza(pizzas, pizzaTeam);
            int idPizza3 = getBestPizza(pizzas, pizzaTeam);

            writer.write(idPizza1 + " ");
            writer.write(idPizza2 + " ");
            writer.write(idPizza3 + "\n");
        }

        for (int i = 0; i < nbOf3PersonTeamServed; i++) {
            Pizza pizzaTeam = new Pizza(pizzas.get(0).getNumberOfIngredients(), pizzas.get(0).getIngredients());
            writer.write("3 " + pizzas.get(0).getId() + " ");
            pizzas.remove(0);

            int idPizza1 = getBestPizza(pizzas, pizzaTeam);
            int idPizza2 = getBestPizza(pizzas, pizzaTeam);

            writer.write(idPizza1 + " ");
            writer.write(idPizza2 + "\n");
        }

        for (int i = 0; i < nbOf2PersonTeamServed; i++) {
            Pizza pizzaTeam = new Pizza(pizzas.get(0).getNumberOfIngredients(), pizzas.get(0).getIngredients());
            writer.write("2 " + pizzas.get(0).getId() + " ");
            pizzas.remove(0);

            int idPizza1 = getBestPizza(pizzas, pizzaTeam);

            writer.write(idPizza1 + "\n");
        }

        writer.close();
    }

    private static int getBestPizza(List<Pizza> pizzas, Pizza pizzaTeam) {
        for (Pizza pizza : pizzas) {
            if (pizzaTeam.addIngredientsDiff(pizza) == pizzaTeam.getNumberOfIngredients()
                    + pizza.getNumberOfIngredients()) {
                pizzaTeam.setNumberOfIngredients(pizzaTeam.getNumberOfIngredients() + pizza.getNumberOfIngredients());
                pizzaTeam.getIngredients().addAll(pizza.getIngredients());
                pizzas.remove(pizza);
                return pizza.getId();
            }
        }

        for (Pizza pizza : pizzas) {
            if (pizzaTeam.addIngredientsDiff(pizza) >= pizzaTeam.getNumberOfIngredients()
                    + (pizza.getNumberOfIngredients() / 2)) {
                pizzaTeam.setNumberOfIngredients(pizzaTeam.getNumberOfIngredients() + pizza.getNumberOfIngredients());
                pizzaTeam.getIngredients().addAll(pizza.getIngredients());
                pizzas.remove(pizza);
                return pizza.getId();
            }
        }

        int id = pizzas.get(0).getId();
        pizzas.remove(0);
        return id;
    }

    private static void displayScore(String[] fileNames) throws IOException {
        int scoreTotal = 0;
        for (String fileName : fileNames) {
            ArrayList<Pizza> pizzas = read(fileName);
            int score = 0;

            BufferedReader reader = new BufferedReader(new FileReader(outputFilesDirectory + fileName + ".out"));

            reader.readLine();

            String line;
            while ((line = reader.readLine()) != null) {
                List<String> ingredientsByTeam = new ArrayList<String>();
                String values[] = line.split(" ");
                for (int i = 1; i < values.length; i++) {
                    for (Pizza pizza : pizzas) {
                        if (pizza.getId() == Integer.valueOf(values[i])) {
                            for (String ingredient : pizza.getIngredients()) {
                                if (!ingredientsByTeam.contains(ingredient)) {
                                    ingredientsByTeam.add(ingredient);
                                }
                            }
                        }
                    }
                }
                score += ingredientsByTeam.size() * ingredientsByTeam.size();
            }

            reader.close();
            scoreTotal += score;
            System.out.println("SCORE FOR " + fileName + ": " + score);
        }
        System.out.println("SCORE TOTAL : " + scoreTotal);
    }
}