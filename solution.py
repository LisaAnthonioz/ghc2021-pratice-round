import time


pizzas = []


class Pizza:
    def __init__(self, id, ingredients, nbIngredients):
        self.id = id
        self.ingredients = ingredients
        self.nbIngredients = nbIngredients


def displayScore(fileNames):
    scoreTotal = 0
    for fileName in fileNames:
        pizzas = read(fileName)

        score = 0
        outputFile = open(outputFilesDirectory + fileName + "_py.out", "rt")

        lines = outputFile.read().splitlines()
        for line in lines[1:]:
            ingredientsByTeam = []
            values = line.split(" ")
            # for pizza in pizzas:
            #     if any(pizza.id == int(values[i]) for i in range(1, len(values))):
            #         ingredientsByTeam.extend(
            #             [ingredient for ingredient in pizza.ingredients if not ingredient in ingredientsByTeam])
            for idx, val in enumerate(values[1:]):
                for pizza in pizzas:
                    if pizza.id == int(val):
                        ingredientsByTeam.extend(
                            value for value in pizza.ingredients if value not in ingredientsByTeam)
                        # for ingredient in pizza.ingredients:
                        #     if not ingredient in ingredientsByTeam:
                        #         ingredientsByTeam.append(ingredient)
            score += len(ingredientsByTeam) * len(ingredientsByTeam)
        outputFile.close()
        scoreTotal += score

        print("SCORE FOR " + fileName + ": " + str(score))

    print("SCORE TOTAL : " + str(scoreTotal))


def read(fileName):
    pizzas = []
    inputFile = open(inputFilesDirectory + fileName + ".in", "rt")
    # Read the file
    lines = inputFile.read().splitlines()
    firstLine = lines[0].strip()

    if firstLine:  # Set the constants
        NUMBER_OF_PIZZAS, NUMBER_OF_2_PERSON_TEAMS, NUMBER_OF_3_PERSON_TEAMS, NUMBER_OF_4_PERSON_TEAMS = list(
            map(int, firstLine.split(' ')))

    idPizza = 0
    for line in lines[1:]:  # Set the pizzas
        # pizzas.append(line.strip().split(' '))§
        values = line.split(" ")
        pizza = Pizza(idPizza, [], int(values[0]))
        for i in range(1, len(values)):
            pizza.ingredients.append(values[i])

        pizzas.append(pizza)
        idPizza += 1

    inputFile.close()
    return pizzas


def process(pizzas, fileName):
    inputFile = open(inputFilesDirectory + fileName + ".in", "rt")
    lines = inputFile.read().splitlines()
    firstLine = lines[0].strip()

    if firstLine:  # Set the constants
        NUMBER_OF_PIZZAS, NUMBER_OF_2_PERSON_TEAMS, NUMBER_OF_3_PERSON_TEAMS, NUMBER_OF_4_PERSON_TEAMS = list(
            map(int, firstLine.split(' ')))

    nbPizzasRestantes = NUMBER_OF_PIZZAS
    nbPizzasRestantesAfter3 = 0
    nbOf4PersonTeamServed = 0
    nbOf3PersonTeamServed = 0
    nbOf2PersonTeamServed = 0
    inputFile.close()

    while (nbPizzasRestantes > 0 and (nbOf3PersonTeamServed < NUMBER_OF_3_PERSON_TEAMS or nbOf4PersonTeamServed < NUMBER_OF_4_PERSON_TEAMS)):

        if nbPizzasRestantes > 1:
            if nbPizzasRestantes > NUMBER_OF_4_PERSON_TEAMS * 4:
                nbOf4PersonTeamServed = NUMBER_OF_4_PERSON_TEAMS
                nbPizzasRestantes = nbPizzasRestantes - NUMBER_OF_4_PERSON_TEAMS * 4
            elif nbPizzasRestantes > 0 and nbPizzasRestantes % 4 == 0:
                nbOf4PersonTeamServed = int(nbPizzasRestantes / 4)
                nbPizzasRestantes = 0

            if nbPizzasRestantes > NUMBER_OF_3_PERSON_TEAMS * 3:
                nbOf3PersonTeamServed = NUMBER_OF_3_PERSON_TEAMS
                nbPizzasRestantes = nbPizzasRestantes - NUMBER_OF_3_PERSON_TEAMS * 3
            elif nbPizzasRestantes > 0 and nbPizzasRestantes % 3 == 0:
                nbOf3PersonTeamServed = int(nbPizzasRestantes / 3)
                nbPizzasRestantes = 0

            if nbPizzasRestantes > NUMBER_OF_2_PERSON_TEAMS * 2:
                nbOf2PersonTeamServed = NUMBER_OF_2_PERSON_TEAMS
                nbPizzasRestantes = nbPizzasRestantes - NUMBER_OF_2_PERSON_TEAMS * 2
            elif nbPizzasRestantes > 0 and nbPizzasRestantes % 2 == 0:
                nbOf2PersonTeamServed = int(nbPizzasRestantes / 2)
                nbPizzasRestantes = 0

        else:
            nbOf4PersonTeamServed -= 1
            nbPizzasRestantes += 4
            if nbPizzasRestantes > NUMBER_OF_3_PERSON_TEAMS * 3:
                nbOf3PersonTeamServed = NUMBER_OF_3_PERSON_TEAMS
                nbPizzasRestantes = nbPizzasRestantes - NUMBER_OF_3_PERSON_TEAMS * 3
            elif nbPizzasRestantes > 0 and nbPizzasRestantes % 3 == 0:
                nbOf3PersonTeamServed = int(nbPizzasRestantes / 3)

            if nbPizzasRestantes > NUMBER_OF_2_PERSON_TEAMS * 2:
                nbOf2PersonTeamServed = NUMBER_OF_2_PERSON_TEAMS
                nbPizzasRestantes = nbPizzasRestantes - NUMBER_OF_2_PERSON_TEAMS * 2
            elif nbPizzasRestantes > 0 and nbPizzasRestantes % 2 == 0:
                nbOf2PersonTeamServed = int(nbPizzasRestantes / 2)
                nbPizzasRestantes = nbPizzasRestantes - NUMBER_OF_2_PERSON_TEAMS * 2

    #  SERVE THE PIZZAS
    nbTeamsDelivered = nbOf4PersonTeamServed + \
        nbOf3PersonTeamServed + nbOf2PersonTeamServed
    outputFile = open(outputFilesDirectory + fileName + "_py.out", "w")
    outputFile.write(str(nbTeamsDelivered) + "\n")

    for i in range(nbOf4PersonTeamServed):
        outputFile.write("4 " + str(pizzas[0].id) + " " + str(pizzas[1].id) + " " + str(pizzas[2].id) + " "
                         + str(pizzas[3].id) + "\n")
        pizzas.remove(pizzas[0])
        pizzas.remove(pizzas[0])
        pizzas.remove(pizzas[0])
        pizzas.remove(pizzas[0])

    for i in range(nbOf3PersonTeamServed):
        outputFile.write(
            "3 " + str(pizzas[0].id) + " " + str(pizzas[1].id) + " " + str(pizzas[2].id) + "\n")
        pizzas.remove(pizzas[0])
        pizzas.remove(pizzas[0])
        pizzas.remove(pizzas[0])

    for i in range(nbOf2PersonTeamServed):
        outputFile.write("2 " + str(pizzas[0].id) +
                         " " + str(pizzas[1].id) + "\n")
        pizzas.remove(pizzas[0])
        pizzas.remove(pizzas[0])

    outputFile.close()
    # Display input data
    # print("---------------------------------------------------------------")
    # print(fileName)
    # print("---------------------------------------------------------------")
    # print(NUMBER_OF_PIZZAS, " pizzas, ", NUMBER_OF_2_PERSON_TEAMS, " team(s) of two, ",
    #       NUMBER_OF_3_PERSON_TEAMS, " team(s) of three, ", NUMBER_OF_4_PERSON_TEAMS, " team(s) of four")
    # print("---------------------------------------------------------------")
    # if int(NUMBER_OF_PIZZAS) < 10:
    #     print("---------------------------------------------------------------")
    #     print(pizzas)
    #     for i in range(int(NUMBER_OF_PIZZAS)):
    #         print("Pizza ", i, " has ", pizzas[i][0], "ingredients : ", end='')
    #         for j in range(1, int(pizzas[i][0])+1):
    #             print(pizzas[i][j], ", ", end='')
    #         print()


def get_nbIngredients(pizza):
    return pizza.nbIngredients


inputFilesDirectory = "input/"
outputFilesDirectory = "output/"

fileNames = ["a_example", "b_little_bit_of_everything", "c_many_ingredients", "d_many_pizzas",
             "e_many_teams"]
# fileNames = ["a_example"]

start_time = time.time() * 1000
for fileName in fileNames:
    pizzas = read(fileName)
    pizzas.sort(key=get_nbIngredients, reverse=True)
    process(pizzas, fileName)

print("Time : ", time.time() * 1000 - start_time, "ms")
displayScore(fileNames)
print("Time : ", time.time() * 1000 - start_time, "ms")
